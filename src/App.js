import * as React from 'react';
import Map, {NavigationControl} from 'react-map-gl';
import Navbar from './components/navbar.js';
import maplibregl from 'maplibre-gl';
import 'maplibre-gl/dist/maplibre-gl.css';
import './App.css';


function App() {
  return (
    <div className="App">
      <Navbar/>
      <Map mapLib={maplibregl} 
        initialViewState={{
          longitude: 47.07389,
          latitude: 8.47176,
          zoom: 14
        }}
        style={{width: "100%", height: " calc(100vh - 77px)"}}
        mapStyle="https://api.maptiler.com/maps/streets-v2/style.json?key=iI4HsAfUaNpw0azb2qQs" 
        >
        <NavigationControl position="top-right" />
      </Map>
    </div>
  );
}

export default App;
